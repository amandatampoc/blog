const express = require('express')
const mongoose = require('mongoose')
const Article = require('./models/article')
const app = express()
const methodOverride = require('method-override')
const articleRouter = require('./routes/articles')

const db = require('./config/keys.js').MongoURI;

// Connect to Mongodb Atlas
mongoose.connect(db, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
    .then(() => console.log('MongoDB Atlas Connected'))
    .catch(err => console.log(err));

app.set('view engine', 'ejs')
app.use(express.urlencoded({ extended: false }))
app.use(methodOverride('_method'))

app.get('/', async (req, res) => {
    const articles = await Article.find().sort({ createdAt: 'desc' })
    res.render('articles/index', { articles: articles })
})

app.use('/articles' , articleRouter)

const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`Server started on port ${PORT}`))